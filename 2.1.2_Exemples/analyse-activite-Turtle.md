# Analyse Module turtle pour introduire python

- Lien: [pdf](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/2_Mise-en-Pratique-Professionnelle/2.1_Penser-Concevoir-Elaborer/2.1.2_Exemples/activites_turtle.pdf?inline=false)

- Objectifs:
    - introduire la syntaxe de python
    - introduire la programmation impérative
    - introduire les boucles for (8.-11.) (et imbriquées (15.))
    - introduire les conditions (16.)
- Pré-requis à cette activité
    - boucles
    - syntaxe des boucles en python
- Durée de l'activité
    - 1h30 jusqu'au 11.
- Exercices cibles
- Description du déroulement de l'activité
    1. Les élèves commencent à lire le sujet et à executer les premières lignes
    2. Au bout de 30minutes iels commencent à arriver au 8
    3. A 45 minutes explication (rappel?) à toute la classe de la boucle for (et de sa syntaxe)
- Anticipation des difficultés des élèves
    - bouger sans rien afficher
    - gestion des angles
    - répétition
    - comment modifier les boucle for et utiliser le `i`
    - boucle for imbriquées
- Gestion de l'hétérogénéïté:
    - Tout le monde doit arriver au moins au 12.
    - Ceux qui arrivent bien continuent jusqu'au bout
    - Si qqun a fini ben bon modifie le dernier pour que ce soit aléatoire la forme déssinée


## Comparaison avec l'analyse originale

C'est une initiation donc il y a peu de pré-requis.
- Penser a où le code est écrit
- Comment sauvegarder un fichier
- Comment mettre des commentaires
- Prendre 10 minutes avant la fin pour sauvegarder
